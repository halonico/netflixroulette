package be.helha.netflixroulette;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import be.helha.netflixroulette.fragment.ItemFragment;
import be.helha.netflixroulette.fragment.ListFragment;
import be.helha.netflixroulette.fragment.SearchFragment;
import be.helha.netflixroulette.model.Actor;
import be.helha.netflixroulette.model.NetflixShow;

public class MainActivity extends AppCompatActivity implements SearchFragment.Listener,ListFragment.Listener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container,SearchFragment.newInstance(this))
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.context_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.home_context:

                for(int i=0;i<getSupportFragmentManager().getBackStackEntryCount();i++) {
                    getSupportFragmentManager().popBackStack();
                }
                break;
            default:
                Log.i("MainActivity","Unknown menu item selected");
        }
        return true;
    }

    @Override
    public void search(String title) {
        String apiUrl = "https://api.themoviedb.org/3/search/movie?api_key=17fec259a0eea27a07f6767ba6bec3e8";

        if(title.length()==0) {
            Toast.makeText(this, "Please enter your search terms first", Toast.LENGTH_SHORT).show();
        }
        else {
            apiUrl += "&query=" + title;

            //Calling singleton request queue for later use
            final RequestQueue rq = RequestQueueSingleton.getInstance(this.getApplicationContext()).getRequestQueue();
            //Making a GET request to NetflixRoller's RESTApi, receiving JSON content as a result
            final JsonObjectRequest jsRequest = new JsonObjectRequest(Request.Method.GET, apiUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.i("MainActivity","OnResponse1");
                            final List<NetflixShow> netflixShowList = new ArrayList<>();
                            try {
                                final JSONArray jsResults = response.getJSONArray("results");
                                for(int i = 0; i<jsResults.length(); i++) {
                                    final NetflixShow ns = generateNetflixShow(jsResults.getJSONObject(i));
                                    netflixShowList.add(ns);
                                    //Credits request
                                    String movieQueryUrl = "https://api.themoviedb.org/3/movie/"+ns.getId()+"?api_key=17fec259a0eea27a07f6767ba6bec3e8&append_to_response=credits";
                                    final JsonObjectRequest jsCastR = new JsonObjectRequest(Request.Method.GET, movieQueryUrl, null,
                                            new Response.Listener<JSONObject>() {

                                                @Override
                                                public void onResponse(JSONObject response) {
                                                    try {
                                                        ns.setRuntime(response.getInt("runtime"));

                                                        //Actors
                                                        JSONArray credits = response.getJSONObject("credits").getJSONArray("cast");
                                                        for(int j = 0; j<credits.length();j++) {
                                                            ns.addActor(new Actor(credits.getJSONObject(j).getString("name"), credits.getJSONObject(j).getString("profile_path")));
                                                        }

                                                        //Director
                                                        JSONArray crew = response.getJSONObject("credits").getJSONArray("crew");
                                                        ns.setDirector(crew.getJSONObject(0).getString("name"));

                                                        //Genres
                                                        JSONArray genres = response.getJSONArray("genres");
                                                        for(int j = 0; j<genres.length(); j++) {
                                                            ns.addGenre(genres.getJSONObject(j).getString("name"));
                                                        }

                                                        //Fragment update
                                                        ListFragment myFragment = (ListFragment) getSupportFragmentManager().findFragmentByTag("LIST_FRAGMENT");
                                                        if (myFragment != null && myFragment.isVisible()) {
                                                            myFragment.updateAdapter();
                                                        }

                                                    } catch (JSONException e) {
                                                        Log.w("MainActivity","Error getting cast data");
                                                    }
                                                }
                                            },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    Toast.makeText(MainActivity.this, "Error getting show details. Please try again", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                    rq.add(jsCastR);
                                }
                                FragmentManager fm = getSupportFragmentManager();
                                FragmentTransaction transaction = fm.beginTransaction();
                                ListFragment tmpfrag = ListFragment.newInstance(MainActivity.this);
                                tmpfrag.setNetflixShowList(netflixShowList);
                                transaction.addToBackStack(null);
                                transaction.replace(R.id.container, tmpfrag, "LIST_FRAGMENT");
                                transaction.commit();
                            }
                            catch (JSONException e) {
                                Log.e("MainActivity","Returned data isn't valid JSON");
                                Toast.makeText(MainActivity.this, "Error: Bad Request\nTry again", Toast.LENGTH_SHORT).show();
                            }
                        }

                        public NetflixShow generateNetflixShow(JSONObject jsShow) {
                            final NetflixShow ns = new NetflixShow();
                            try {
                                ns.setId(jsShow.getInt("id"));
                                ns.setOverview(jsShow.getString("overview"));
                                ns.setPosterPath("http://image.tmdb.org/t/p/w185/"+jsShow.getString("poster_path"));
                                ns.setTitle(jsShow.getString("title"));
                                ns.setVote_avg(jsShow.getDouble("vote_average"));
                                ns.setReleaseDate(jsShow.getString("release_date"));
                            }
                            catch (JSONException e) {

                            }
                            return ns;
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(MainActivity.this, "An error occured.\nPlease try again", Toast.LENGTH_SHORT).show();
                            Log.e("MainActivity", "Search process error: " + error.getMessage());
                        }
                    }

            );
            //Executing the request
            rq.add(jsRequest);
        }
    }

    @Override
    public void show(NetflixShow netflixShow) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        ItemFragment iFragment = new ItemFragment();
        iFragment.setNetflixShow(netflixShow);
        transaction.addToBackStack(null);
        transaction.replace(R.id.container, iFragment);
        transaction.commit();

    }
}
