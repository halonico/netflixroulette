package be.helha.netflixroulette.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import be.helha.netflixroulette.R;
import be.helha.netflixroulette.model.Actor;
import be.helha.netflixroulette.model.MediaType;
import be.helha.netflixroulette.model.NetflixShow;

public class ItemFragment extends Fragment {

    private TextView tvTitle;
    private TextView tvType;
    private TextView tvLength;
    private TextView tvDirector;
    private TextView tvActors;
    private TextView tvSummary;
    private ImageView ivImage;
    private RatingBar rbRating;
    private NetflixShow ns;

    public ItemFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_item, container, false);
        tvTitle = v.findViewById(R.id.tv_title_fitem);
        tvType = v.findViewById(R.id.tv_type_fitem);
        tvLength = v.findViewById(R.id.tv_length_fitem);
        tvDirector = v.findViewById(R.id.tv_director_fitem);
        tvActors = v.findViewById(R.id.tv_actors_fitem);
        tvSummary = v.findViewById(R.id.tv_summary_fitem);
        ivImage = v.findViewById(R.id.iv_image_fitem);
        rbRating = v.findViewById(R.id.rb_rating_fitem);

        fillFields(ns,v);

        return v;
    }

    public void fillFields(NetflixShow ns, View v) {
        tvTitle.setText(ns.getTitle());
        tvType.setText(ns.showGenres());
        tvLength.setText(ns.getRuntime()+" min.");
        tvDirector.setText("Directed by "+ns.getDirector());
        //Actors list toString
        String actorString;

        if(ns.getActors().size()==0) {
             actorString = "No actors found for this show.";
        }
        else {
            actorString = "With ";
            for (Actor a : ns.getActors()) {
                actorString += a;
                if (ns.getActors().indexOf(a) < ns.getActors().size() - 2) {
                    actorString += ", ";
                } else if (ns.getActors().indexOf(a) < ns.getActors().size() - 1) {
                    actorString += " and ";
                }
            }
        }
        tvActors.setText(actorString);
        tvSummary.setText(ns.getOverview());
        Picasso.with(v.getContext()).load(ns.getPosterPath()).error(R.drawable.defaultimage).into(ivImage);
        rbRating.setRating((float)ns.getVote_avg()/2f);
    }

    public void setNetflixShow(NetflixShow ns) {
        this.ns = ns;
    }

}
