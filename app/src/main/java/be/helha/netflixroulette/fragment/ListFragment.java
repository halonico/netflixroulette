package be.helha.netflixroulette.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;

import be.helha.netflixroulette.R;
import be.helha.netflixroulette.model.MyCustomAdapter;
import be.helha.netflixroulette.model.NetflixShow;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment implements MyCustomAdapter.OnActionCompleted{
    private RecyclerView recyclerView;
    private List<NetflixShow> netflixShowList;
    private Listener listener;

    public interface Listener
    {
        void show(NetflixShow netflixShow);
    }

    public ListFragment() {
        // Required empty public constructor
    }

    public static ListFragment newInstance(Listener listener)
    {
        ListFragment listFragment = new ListFragment();
        listFragment.setListener(listener);
        return(listFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = v.findViewById(R.id.rv_list_flist);
        recyclerView.setHasFixedSize(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new MyCustomAdapter(this.getContext(),netflixShowList,ListFragment.this));
        return v;
    }

    public List<NetflixShow> getNetflixShowList() {
        return netflixShowList;
    }

    public void setListener(Listener listener)
    {
        this.listener=listener;
    }

    public void setNetflixShowList(List<NetflixShow> netflixShowList) {
        this.netflixShowList = netflixShowList;
    }

    public void updateAdapter() {
        recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void OnClick(NetflixShow netflixShow) {
        listener.show(netflixShow);
    }
}
