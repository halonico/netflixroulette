package be.helha.netflixroulette.fragment;


import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import be.helha.netflixroulette.R;
import be.helha.netflixroulette.model.NetflixShow;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements View.OnClickListener {
    private EditText etTitle;
    private TextView tvTitle;
    private Button btnSearch;
    private Listener listener;

    public interface Listener
    {
        void search(String title);
    }

    public void setListener(Listener listener)
    {
        this.listener=listener;
    }

    public static SearchFragment newInstance(Listener listener)
    {
        SearchFragment searchFragment = new SearchFragment();
        searchFragment.setListener(listener);
        return(searchFragment);
    }
    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_search, container, false);
        etTitle = v.findViewById(R.id.et_title_fsearch);
        tvTitle = v.findViewById(R.id.tv_title_fsearch);
        btnSearch = v.findViewById(R.id.btn_search_fsearch);
        btnSearch.setOnClickListener(this);
        tvTitle.setOnClickListener(this);
        tvTitle.performClick();
        return v;

    }
    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.btn_search_fsearch:
                if(listener!=null)
                {
                    String title=etTitle.getText().toString();
                    listener.search(title);
                }
                else
                {
                    Log.w("SearchFragment","No fragment listener");
                }
                break;
            case R.id.tv_title_fsearch:
                tvTitle.setTextColor(getResources().getColor(R.color.colorAccent));
                break;
            default:
                Log.w("SearchFragment","Unhandled button click");
                break;
        }
    }
}
