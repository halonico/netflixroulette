package be.helha.netflixroulette;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * This class keeps an unique instance of a RequestMethod for better network performance.
 */

public class RequestQueueSingleton {
    private static RequestQueueSingleton instance;
    private RequestQueue rq;
    private static Context ctxt;

    private RequestQueueSingleton(Context ctxt) {
        this.ctxt = ctxt;
        this.rq = getRequestQueue();

        //Ability to load/cache images as well
        //TODO Add image code
    }

    //Singleton getter
    public static synchronized RequestQueueSingleton getInstance(Context context) {
        if(instance==null) {
            instance = new RequestQueueSingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if(rq == null) {
            rq = Volley.newRequestQueue(ctxt.getApplicationContext());
        }
        return rq;
    }

    public <T> void addRequestToRQ(Request<T> req) {
        getRequestQueue().add(req);
    }

}
