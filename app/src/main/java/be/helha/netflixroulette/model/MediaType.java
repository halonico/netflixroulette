package be.helha.netflixroulette.model;

public enum MediaType {
    MOVIE   ("Movie"),
    SERIES  ("Series");

    private final String typeString;

    MediaType(String typeString)  {
        this.typeString = typeString;
    }

    @Override
    public String toString() {
        return typeString;
    }
}
