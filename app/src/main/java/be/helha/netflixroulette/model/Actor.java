package be.helha.netflixroulette.model;

public class Actor {
    private String name;
    private String poster;

    public Actor(String name, String poster) {
        this.name = name;
        this.poster = poster;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public String toString() {
        return name;
    }
}


