package be.helha.netflixroulette.model;

import java.util.ArrayList;
import java.util.List;

public class NetflixShow {

    private double vote_avg;
    private String title;
    private String posterPath;
    private String overview;
    private String releaseDate;
    private int id;
    private int runtime;
    private List<Actor> actors;
    private String director;
    private List<String> genres;

    public NetflixShow() {
        actors = new ArrayList<>();
        genres = new ArrayList<>();
    }

    public double getVote_avg() {
        return vote_avg;
    }

    public void setVote_avg(double vote_avg) {
        this.vote_avg = vote_avg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int   getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public void addActor(Actor a) {
        actors.add(a);
    }

    public String getDirector() {
        return director;
    }

    public void addGenre(String genre) {
        genres.add(genre);
    }

    public String showGenres() {
        String genreList="";

        if(genres.size()>0) {

            for(String g : genres) {
                genreList += g;
                if (genres.indexOf(g) < genres.size() - 1) {
                    genreList+=", ";
                }
            }
        }
        else {
            genreList+="No genres found for this show.";
        }
        return genreList;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "NetflixShow{" +
                "vote_avg=" + vote_avg +
                ", title='" + title + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", overview='" + overview + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", id=" + id +
                ", runtime=" + runtime +
                ", actors=" + actors +
                '}';
    }
}
