package be.helha.netflixroulette.model;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import be.helha.netflixroulette.MainActivity;
import be.helha.netflixroulette.R;
import be.helha.netflixroulette.fragment.ListFragment;

public class MyCustomAdapter extends RecyclerView.Adapter<MyCustomAdapter.MyViewHolder> {
    private Context context;
    private List<NetflixShow> netflixShowList;
    private OnActionCompleted callback;
    public MyCustomAdapter(Context context, List<NetflixShow> netflixShowList,OnActionCompleted callback) {
        this.netflixShowList = netflixShowList;
        this.context=context;
        this.callback=callback;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View v = LayoutInflater.from(context).inflate(R.layout.item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NetflixShow item = netflixShowList.get(position);
        holder.tvItemTitle.setText(item.getTitle());
        holder.tvItemDirector.setText(item.getDirector());
        holder.tvItemLength.setText(item.getRuntime()+" min.");
        holder.tvItemYear.setText(item.getReleaseDate());
        holder.rbItemRating.setRating(((float) item.getVote_avg()/2f));
        Picasso.with(context).load(item.getPosterPath()).error(R.drawable.defaultimage).into(holder.ivItemImage);
    }

    @Override
    public void onViewRecycled(MyViewHolder holder) {
        super.onViewRecycled(holder);

    }

    @Override
    public int getItemCount() {
        return netflixShowList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvItemTitle,tvItemDirector,tvItemYear, tvItemLength;
        private RatingBar rbItemRating;
        private ImageView ivItemImage;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.tvItemTitle = itemView.findViewById(R.id.tv_title_item);
            this.tvItemDirector = itemView.findViewById(R.id.tv_director_item);
            this.tvItemYear = itemView.findViewById(R.id.tv_release_item);
            this.rbItemRating = itemView.findViewById(R.id.rb_rating_item);
            this.ivItemImage = itemView.findViewById(R.id.iv_image_item);
            this.tvItemLength =itemView.findViewById(R.id.tv_length_item);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            callback.OnClick(netflixShowList.get(getAdapterPosition()));
        }

    }
    public interface OnActionCompleted {
        void OnClick(NetflixShow netflixShow);
    }
}
